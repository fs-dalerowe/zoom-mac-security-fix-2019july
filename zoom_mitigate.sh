#!/bin/bash
echo Checking if root:
if [ "$EUID" -ne 0 ]
  then echo "Please run as root (sudo ./zoom_mitigate.sh)"
  exit
fi
echo UID is 0... Proceeding

#CVE-2019–13449 (DOS fixed in 4.4.2)
#CVE-2019–13450 (Info Disclosure - Webcam)
#No CVE - removes backdoor webserver
# Script adapted from https://gist.github.com/JLLeitschuh/e2550ddd8d6dfd94447b0b557891ba30#file-permanent_zoom_and_ringcentral_server_remover-sh and https://gist.github.com/JLLeitschuh/ff6b1d57e68a6993921b196c95808ad6#file-disable_video_by_default_for_zoom-sh

echo Disabling automatic video on meeting join...
# Disable autovideo on meeting join
defaults write /Library/Preferences/us.zoom.config.plist ZDisableVideo 1

echo Killing webserver process...
# Kill the webserver backdoor:
kill -9 $(lsof -ti tcp:19421)

echo Removing webserver files and blocking with chmod 000 ~/.zoomus 
# Prevent backdoor webserver reinstalling during updates
pkill "ZoomOpener"; rm -rf ~/.zoomus; touch ~/.zoomus && chmod 000 ~/.zoomus;
pkill "RingCentralOpener";  rm -rf ~/.ringcentralopener; touch ~/.ringcentralopener && chmod 000 ~/.ringcentralopener;

echo Done.  This script should be run for each user if multiple users use this Mac.